var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var deleteLines = require('gulp-delete-lines');
var htmlmin = require('gulp-htmlmin');
var sourcemaps = require('gulp-sourcemaps');
var tinypng = require('gulp-tinypng-compress');
var uglify = require('gulp-uglify');

gulp.task('sass', function () {
    return gulp.src('./src/sass/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            // nested, expanded, compact, compressed
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('js', function () {
    return gulp.src('./src/js/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('all.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist/js'));
});

gulp.task('tinypng', function () {
    return gulp.src('./src/img/**/*.{png,jpg,jpeg}')
        .pipe(tinypng({
            key: '6_2Zvi5VbHQSrJKPfShhwUz5CjDdD97l',
            sigFile: './src/img/.tinypng-sigs',
            log: true
        }))
        .pipe(gulp.dest('./dist/img'));
});

gulp.task('htmlmin', function () {
    return gulp.src(['./src/**/*.html', '!./src/**/*.tpl.html'])
        .pipe(deleteLines({
            'filters': [
                /<link href="\.\.\/dist\/css\/styles.css" rel="stylesheet">/g
            ]
        }))
        .pipe(deleteLines({
            'filters': [
                /<script src="\.\.\/dist\/js\/all\.js"><\/script>/g
            ]
        }))
        .pipe(htmlmin({
            collapseWhitespace: true,
            minifyCSS: true,
            minifyJS: true,
            removeComments: true
        }))
        .pipe(gulp.dest('./dist'));
});

gulp.task('templateshtmlmin', function () {
    return gulp.src('./src/**/*.tpl.html')
        .pipe(htmlmin({
            collapseWhitespace: true,
            minifyCSS: true,
            minifyJS: true,
            removeComments: true
        }))
        .pipe(gulp.dest('./dist'));
});

gulp.task('copy-files', function () {
    var copy = {
        files: ['./src/*.ico', './src/*.png', './src/.htaccess']
    };
    return gulp.src(copy.files, {
            base: "./src/"
        })
        .pipe(gulp.dest('./dist'));
});

gulp.task('build', ['sass', 'js', 'tinypng', 'htmlmin', 'templateshtmlmin', 'copy-files'], function () {
    gulp.watch('./src/sass/**/*.scss', ['sass']);
    gulp.watch('./src/js/**/*.js', ['js']);
    gulp.watch('./src/**/*.html', ['htmlmin', 'templateshtmlmin']);
});