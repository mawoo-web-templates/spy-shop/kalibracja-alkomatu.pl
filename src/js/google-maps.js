if (window.document.getElementsByTagName('article')[0]) {
    var articleName = document.getElementsByTagName('article')[0].getAttribute('data-site');
}

if (new RegExp('city-', 'g').test(articleName) === true) {
    // Mapy Google i Google Street View
    var city = articleName.replace('city-', '');

    var address = {
        bydgoszcz: {
            lat: 53.1296307,
            lng: 18.0025372,
            pano: {
                lat: 53.1295721,
                lng: 18.0025446,
                heading: -180,
                pitch: 0
            }
        },
        gdynia: {
            lat: 54.512747,
            lng: 18.5382049,
            pano: {
                lat: 54.5132475,
                lng: 18.5382227,
                heading: 80,
                pitch: 0
            }
        },
        katowice: {
            lat: 50.260178,
            lng: 19.019764,
            pano: {
                lat: 50.2604745,
                lng: 19.019579,
                heading: -100,
                pitch: 0
            }
        },
        krakow: {
            lat: 50.069847,
            lng: 19.936767,
            pano: {
                lat: 50.069911,
                lng: 19.9367228,
                heading: -140,
                pitch: 0
            }
        },
        poznan: {
            lat: 52.3993267,
            lng: 16.9044798,
            pano: {
                lat: 52.3993757,
                lng: 16.9044893,
                heading: -90,
                pitch: 0
            }
        },
        rzeszow: {
            lat: 50.0328727,
            lng: 21.9986592,
            pano: {
                lat: 50.03293,
                lng: 21.9986167,
                heading: -90,
                pitch: 0
            }
        },
        szczecin: {
            lat: 53.4275249,
            lng: 14.533324,
            pano: {
                lat: 53.4275892,
                lng: 14.5333723,
                heading: -200,
                pitch: 0
            }
        },
        wroclaw: {
            lat: 51.0977779,
            lng: 17.0557197,
            pano: {
                lat: 51.0977549,
                lng: 17.0558153,
                heading: -45,
                pitch: 0
            }
        }
    };

    function initMap() {
        var mapCoordinates = {
            lat: address[city].lat,
            lng: address[city].lng
        };
        var panoCoordinates = {
            lat: address[city].pano.lat,
            lng: address[city].pano.lng
        };
        var map = new google.maps.Map(document.getElementById('calibration-point-map'), {
            center: mapCoordinates,
            scrollwheel: false,
            zoom: 15
        });
        var panorama = new google.maps.StreetViewPanorama(document.getElementById('calibration-point-street-view'), {
            position: panoCoordinates,
            pov: {
                heading: address[city].pano.heading,
                pitch: address[city].pano.pitch
            },
            scrollwheel: false,
            motionTracking: false,
            zoom: 0
        });
        map.setStreetView(panorama);

        var marker = new google.maps.Marker({
            position: mapCoordinates,
            map: map
        });
    }
};