(function ($) {
    $(window).on('load', function () {
        var $status = $('#table-of-contents').length;

        if ($status === 1) {
            var html = '<p class="h2">W tym artykule znajdziesz:</p>';
            html += '<ul>';

            $('article h2, article h3').each(function (index) {
                var $menuItem = $(this).text();
                $(this).attr('id', 'item-' + (index + 1));
                html += '<li><a class="scroll" href="#item-' + (index + 1) + '">' + $menuItem + '</a></li>';
            });

            html += '</ul>';

            $('#table-of-contents').append(html);
        }
    });
}(jQuery));