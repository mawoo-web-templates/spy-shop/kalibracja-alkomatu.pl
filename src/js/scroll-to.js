(function ($) {
    $(document).ready(function () {

        function animatedScroll(el) {
            var $section = el.attr('href');
            $('html, body').animate({
                scrollTop: $($section).offset().top - 30
            }, 500, function () {
                $(this).clearQueue();
            });
        }

        $('body').on('click', 'a.scroll', function (event) {
            event.preventDefault();
            var $el = $(this);
            animatedScroll($el);
        });
    });
}(jQuery));