(function ($) {
    $(window).on('load', function () {
        var $maxHeight = 0;
        $('.myths .heading').each(function () {
            var $height = $(this).outerHeight();
            if ($height > $maxHeight) {
                $maxHeight = $height;
            };
        });
        $('.myths .heading').css('height', $maxHeight);
    });
}(jQuery));