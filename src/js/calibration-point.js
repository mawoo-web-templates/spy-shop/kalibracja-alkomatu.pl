(function ($) {
    $(window).on('load', function () {
        var address = [{
            city: 'Bydgoszcz',
            street: 'Jana i Jędrzeja Śniadeckich 19',
            zip: '85-011',
            phone: '+48 52 346 64 64',
            email: 'bydgoszcz@spyshop.pl',
            url: 'kalibracja-alkomatow-bydgoszcz.html'
        }, {
            city: 'Gdynia',
            street: 'Władysława IV 41',
            zip: '81-395',
            phone: '​+48 58 621 92 20',
            email: 'gdynia@spyshop.pl',
            url: 'kalibracja-alkomatow-gdynia.html'
        }, {
            city: 'Katowice',
            street: 'Mickiewicza 14',
            zip: '40-092',
            phone: '​+48 32 258 51 51',
            email: 'katowice@spyshop.pl',
            url: 'kalibracja-alkomatow-katowice.html'
        }, {
            city: 'Kraków',
            street: 'Długa 48 (w oficynie)',
            zip: '31-146',
            phone: '​+48 12 633 45 55',
            email: 'krakow@spyshop.pl',
            url: 'kalibracja-alkomatow-krakow.html'
        }, {
            city: 'Poznań',
            street: '​Głogowska 32',
            zip: '​60-736',
            phone: '​+48 61 862 65 55',
            email: 'poznan@spyshop.pl',
            url: 'kalibracja-alkomatow-poznan.html'
        }, {
            city: 'Rzeszów',
            street: '​płk. Leopolda Lisa-Kuli 1',
            zip: '​35-032',
            phone: '​+48 17 250 09 07',
            email: 'rzeszow@spyshop.pl',
            url: 'kalibracja-alkomatow-rzeszow.html'
        }, {
            city: 'Szczecin',
            street: '​Bolesława Krzywoustego 51',
            zip: '​70-317',
            phone: '​+48 91 484 04 04',
            email: 'szczecin@spyshop.pl',
            url: 'kalibracja-alkomatow-szczecin.html'
        }, {
            city: 'Wrocław',
            street: '​Traugutta 143',
            zip: '​50-419',
            phone: '​+48 71 341 50 50',
            email: 'wroclaw@spyshop.pl',
            url: 'kalibracja-alkomatow-wroclaw.html'
        }];


        function init(idx) {
            $('#cities li, #map > .markers i').removeClass('active');
            $('#map > .markers i:eq(' + idx + '), #cities li:eq(' + idx + ')').addClass('active');
        }

        function scrollToMap() {
            $('html, body').animate({
                scrollTop: $('#map').offset().top - 30
            }, 500, function () {
                $(this).clearQueue();
            });
        }

        function showAddress(idx, pos, w) {
            $('#map > .address').remove();
            $('#map').append('<div class="address"><span class="city"><a href="' + address[idx].url + '" rel="nofollow">Spy Shop ' + address[idx].city + '</a></span><span>' + address[idx].street + '</span><span>' + address[idx].zip + ', ' + address[idx].city + '</span><span>Tel.: <a href="tel:' + address[idx].phone + '">' + address[idx].phone + '</a></span><span>E-mail: <a href="mailto:' + address[idx].email + '">' + address[idx].email + '</a></span><span class="close"><i class="fa fa-times" aria-hidden="true"></i></span></div>');
            if (w >= 768) {
                $('#map > .address').css({
                    left: pos.left + 50,
                    position: 'absolute',
                    top: pos.top - 18
                });
            }
        }

        $('#cities li, #map > .markers > i').on('click', function () {
            var $index = $(this).index();
            var $pos = $('#map > .markers i:eq(' + $index + ')').position();
            var $windowWidth = $(window).width();

            if ($(this).hasClass('active') == false) {
                init($index);
                showAddress($index, $pos, $windowWidth);
            }
            scrollToMap();
        });

        $('#map').on('click', '.close', function () {
            $('#map > .address').remove();
            $('#cities li, #map > .markers i').removeClass('active');
        });
    });
}(jQuery));