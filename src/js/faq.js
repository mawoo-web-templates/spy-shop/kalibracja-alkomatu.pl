(function ($) {
	$(window).ready(function () {
		var el = {
			plus: 'fa fa-plus',
			minus: 'fa fa-minus'
		}

		function openPanel() {
			var hash = window.location.hash;
			var pathName = window.location.pathname;
			if (hash !== '' && pathName === '/poradnik.html') {
				var panel = hash.replace('heading', 'collapse');
				$(panel).addClass('in');
				$(hash).addClass('active');
			} else {
				$('#collapse-1').addClass('in');
				$('#heading-1').addClass('active');
			}
		}

		function addIcons() {
			$('#accordion .panel-collapse').each(function () {
				if ($(this).is(':visible')) {
					$(this).parent().find('.panel-title > a').append('<span class="' + el.minus + '" aria-hidden="true"></span>');
				} else {
					$(this).parent().find('.panel-title > a').append('<span class="' + el.plus + '" aria-hidden="true"></span>');
				}
			});
		}

		function changeIcons($this) {
			if ($this.parents('.panel').find('.panel-collapse').is(':hidden')) {
				$('#accordion .panel-title > a > span').attr('class', el.plus);
				$this.find('span').attr('class', el.minus);
			} else {
				$this.find('span').attr('class', el.plus);
			}
			scrollTo($this);
		}

		function changePanelHeadingColor($this) {
			if ($this.parents('.panel').find('.panel-collapse').is(':hidden')) {
				$('#accordion .panel-heading').removeClass('active');
				$this.parents('.panel').find('.panel-heading').addClass('active');
			} else {
				$this.parents('.panel').find('.panel-heading').removeClass('active');
			}
		}

		function scrollTo($this) {
			var $el = $this.parents('.panel-heading').attr('id');

			setTimeout(function () {
				$('html, body').animate({
					scrollTop: $('#' + $el).offset().top - 10
				}, 500);
			}, 500);
		}

		openPanel();
		addIcons();

		$('body').on('click', '#accordion .panel-title > a', function () {
			changeIcons($(this));
			changePanelHeadingColor($(this));
		});
	});
}(jQuery));