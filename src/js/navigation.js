(function ($) {
    $(document).ready(function () {

        // Touchscreen
        var touchStatus = ('ontouchstart' in document.documentElement);
        if (touchStatus !== true) {
            $('#navigation').addClass('desktop');
        }

        // Touchscreen menu
        $('.navigation-toggle').on('click', function () {
            var $menu = $('.navigation-menu .main');
            var $submenu = $('.navigation-menu .submenu');
            var $height = $menu.outerHeight();

            if ($submenu.length >= 1) {
                $($submenu).each(function () {
                    if ($(this).find('> .heading').length === 0) {
                        var $name = $(this).parent().find('> .drop-down').text();
                        $(this).prepend('<li class="heading"><span class="drop-down-toggle back"></span><span class="name">' + $name + '</span></li>');
                    }
                    if ($(this).parent().find('> .drop-down-toggle.next').length < 1) {
                        $(this).parent().find('> .drop-down').after('<span class="drop-down-toggle next"></span>');
                    }
                    if ($height < $(this).outerHeight()) {
                        $height = $(this).outerHeight();
                    }
                });
            }
            $menu.css('height', $height);
            $submenu.css('height', '100%');
            $menu.toggleClass('active');
        });

        function scrollToTop() {
            $('html, body').animate({
                scrollTop: 0
            }, 500, function () {
                $(this).clearQueue();
            });
        }

        $('#navigation').on('click', '.drop-down-toggle.next', function () {
            $(this).next().addClass('active');
            scrollToTop();
        });
        $('#navigation').on('click', '.drop-down-toggle.back', function () {
            $(this).parent().parent('.submenu').removeClass('active');
            scrollToTop();
        });

        $(window).on('resize', function () {
            if ($(this).width() >= 1200) {
                $('#navigation .heading, #navigation .drop-down-toggle').remove();
            }
        });
    });
}(jQuery));