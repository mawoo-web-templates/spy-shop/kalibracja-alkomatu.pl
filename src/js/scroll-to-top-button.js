(function ($) {
    $(document).ready(function () {
        var $windowScrollTop = $(window).scrollTop();

        function initScrollToTop(st) {
            $('body').append('<div class="scroll-to-top"></div>');
            showScrollToTop(st);
        }

        function showScrollToTop(st) {
            if (st >= 300) {
                $('.scroll-to-top').addClass('active');
            } else {
                $('.scroll-to-top').removeClass('active');
            }
        }

        function animateScroll() {
            $('html, body').animate({
                scrollTop: 0
            }, 500, function () {
                $(this).clearQueue();
            });
        }

        initScrollToTop($windowScrollTop);

        $(window).on('scroll', function () {
            $windowScrollTop = $(window).scrollTop();
            showScrollToTop($windowScrollTop);
        });

        $('.scroll-to-top').on('click', animateScroll);
    });
}(jQuery));